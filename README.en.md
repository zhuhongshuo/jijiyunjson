# jijiyunjson

#### Description
一个快速java json库，同样的数据量的情况下比JSON-lib快5倍，在小数据量的情况下比阿里巴巴json库fastjson快5倍。
而代码量只有同类产品十分之一代码量只有几千行。作为我们团队第一个开源产品，后续会保持更新，敬请期待。
——————吉极云出品必是精品。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

java.math.BigDecimal decimal = new BigDecimal("+12.010");

if(decimal instanceof BigDecimal) { System.out.println(decimal.toPlainString()); }

// 返回参数错误 StatusData currentStatusData=new StatusData();

currentStatusData.setErrorCode(StatusData.PARAM_ERROR);

Json jsonObject=Json.getInstance();

String str=jsonObject.jsonToStr(currentStatusData);

System.out.println(str);

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
