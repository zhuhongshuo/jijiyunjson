# jijiyunjson

#### 介绍
一个快速java json库，同样的数据量的情况下比JSON-lib快5倍，在小数据量的情况下比阿里巴巴json库fastjson快5倍。
而代码量只有同类产品十分之一代码量只有几千行。作为我们团队第一个开源产品，后续会保持更新，敬请期待。
——————吉极云出品必是精品。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

java.math.BigDecimal decimal = new BigDecimal("+12.010");

if(decimal instanceof BigDecimal) {
    System.out.println(decimal.toPlainString());
}

// 返回参数错误
StatusData currentStatusData=new StatusData();

currentStatusData.setErrorCode(StatusData.PARAM_ERROR);

Json jsonObject=Json.getInstance();

String str=jsonObject.jsonToStr(currentStatusData);

System.out.println(str);

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
